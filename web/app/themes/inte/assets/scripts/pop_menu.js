function show_hide_pop_menu() {
    var links = document.getElementById("myLinks");
    if (links.style.display === "block") {
      links.style.display = "none";
    } else {
      links.style.display = "block";
      links.style.flexDirection = "column";
      links.style.alignIitems = "flex-start";
      links.style.padding = "20px";
      links.style.left = "0px";
      links.style.top = "0px";
      links.style.background = "white";
      links.style.width = "85%";
      links.style.height = "auto";
      links.style.position = "absolute";
    }
}