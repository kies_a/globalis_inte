
var slideIndex = 1;
window.addEventListener("DOMContentloaded", () => {
  
  showSlides(slideIndex);

});

function plusSlides(n) {
  showSlides((slideIndex += n));
}

function currentSlide(n) {
  showSlides((slideIndex = n));
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("slider_card_mobile");
  var dots = document.getElementsByClassName("slider_dot");
    if (n > slides.length) {
      slideIndex = 1;
    }
    if (n < 1) {
      slideIndex = slides.length;
    }
    for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
    }
    for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
    }
    slides[slideIndex - 1].style.display = "block";
    dots[slideIndex - 1].className += " active";
}

function nextFormations() {
  var i;
  var slides = document.getElementsByClassName("card_desktop");
  for(i = 0; i < 3; i++) slides[i].style.display = "none";
  for(i = 3; i < 6; i++) slides[i].style.setProperty('display', 'block', 'important');

  document.getElementById("right_arrow").style.opacity = "0.24";
  document.getElementById("left_arrow").style.opacity = "1";

}


function prevFormations() {
  var i;
  var slides = document.getElementsByClassName("card_desktop");
  for(i = 3; i < 6; i++) slides[i].style.display = "none";
  for(i = 0; i < 3; i++) slides[i].style.setProperty('display', 'block', 'important');
  document.getElementById("right_arrow").style.opacity = "1";
  document.getElementById("left_arrow").style.opacity = "0.24";
}