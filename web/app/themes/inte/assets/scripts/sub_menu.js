 function show_hide_sub_menu() {
    var links = document.getElementById("links_for_domaines");
    var icon = document.getElementById("plus_icon");
    if (links.style.display === "block") {
      links.style.display = "none";
      icon.src =  "app/themes/inte/assets/images/plus.svg";
    } else {
      links.style.display = "block";
      links.style.flexDirection = "column";
      links.style.alignIitems = "flex-start";
      links.style.padding = "20px";
      links.style.left = "0px";
      links.style.top = "0px";
      links.style.background = "white";
      links.style.width = "auto";
      links.style.height = "100%";
      links.style.position = "relative";
      icon.src =  "app/themes/inte/assets/images/minus.svg";
    }
}