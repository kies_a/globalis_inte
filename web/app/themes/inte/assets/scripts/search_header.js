function show_hide_search_header() {
    var search = document.getElementById("search_header");
    if (search.style.display === "inline-block") {
      search.style.display = "none";
    } else {
      search.style.display = "inline-block";
    }
}