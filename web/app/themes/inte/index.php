<!doctype html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php get_header(); ?>
    </head>
    <body>
        <div class="important_container">
            <div class="important_logo">
                <img  src="<?php echo bloginfo('template_url'); ?>/assets/images/important.svg" />
                <p class="important_p">Ukraine: ifocop affirme sa solidarité avec les Ukraniens</p>
            </div>
            <a href="#" class="important_a">
                En savoir plus
            </a>
        </div>
        <div class="informations">
            <div class="informations_container">
                <p class="informations_header2">bienvenue chez IFOCOP!</p>
                <p class="informations_header1">Vivre une aventure formative, apprendre tout au long de sa vie</p>
                <p class="informations_p">
                    Démarrer une formation professionnelle chez ifocop, c'est optimiser ses chances d'employabilité grâce à des certifications RNCP
                    reconnues par les entreprises et des compétences-métiers prisées.
                </p>
                <button class="informations_button">
                    Participer à une réunion d'information
                </button>
            </div>
            <div class="informations_global_container">
                <div class="informations_container_images">
                    <img class="informations_container_images_img" src="<?php echo bloginfo('template_url'); ?>/assets/images/imgsInfo.svg" alt="" srcset="">
                    <img class="informations_container_images_img" src="<?php echo bloginfo('template_url'); ?>/assets/images/union.svg" alt="" srcset="">
                </div>
                <div class="informations_container_images">
                    <img class="informations_container_images_img" src="<?php echo bloginfo('template_url'); ?>/assets/images/peopleInfo.svg" alt="" srcset="">
                    <img class="informations_container_images_img" src="<?php echo bloginfo('template_url'); ?>/assets/images/imgInfo.svg" alt="" srcset="">
                </div>
            </div>
        </div>
        <div class="certs">
            <div class="certs_one_cert">
                <img class="certs_one_logo"  src="<?php echo bloginfo('template_url'); ?>/assets/images/certificate.svg" />
                <p class="certs_one_title">Certifications RNCP</p>
                <p class="certs_one_description">Des offres de qualité, avec un catalogue de 23 certifications RNCP éligibles aux différents dispositifs
                    de financement, pour permettre à chacun de se former, quel que soit son statut
                </p>
            </div>
            <div class="certs_one_cert">
                <img class="certs_one_logo"  src="<?php echo bloginfo('template_url'); ?>/assets/images/tools.svg" />
                <p class="certs_one_title">Immersion en entreprise</p>
                <p class="certs_one_description">Des offres de qualité, avec un catalogue de 23 certifications RNCP éligibles aux différents dispositifs
                    de financement, pour permettre à chacun de se former, quel que soit son statut
                </p>
            </div>
            <div class="certs_one_cert">
                <img class="certs_one_logo"  src="<?php echo bloginfo('template_url'); ?>/assets/images/people.svg" />
                <p class="certs_one_title">Accompagnement humain</p>
                <p class="certs_one_description">Des offres de qualité, avec un catalogue de 23 certifications RNCP éligibles aux différents dispositifs
                    de financement, pour permettre à chacun de se former, quel que soit son statut
                </p>
            </div>
        </div>
        <div class="formations">
            <p class="formations_p">Quel domaine de formation professionnelle vous intéresse ?</p>
            <div class="formations_container_all_formation">
                <div class="formations_container_one_formation">
                    <img  src="<?php echo bloginfo('template_url'); ?>/assets/images/Achats.svg" />
                    <p class="formations_a">Achats</p>
                </div>
                <div class="formations_container_one_formation">
                    <img  src="<?php echo bloginfo('template_url'); ?>/assets/images/Assistanat_Secretariat.svg" />
                    <p class="formations_a">Assistanat Secrétariat</p>
                </div>
                <div class="formations_container_one_formation">
                    <img  src="<?php echo bloginfo('template_url'); ?>/assets/images/Commerce.svg" />
                    <p class="formations_a">Commerce</p>
                </div>
                <div class="formations_container_one_formation">
                    <img  src="<?php echo bloginfo('template_url'); ?>/assets/images/Commerce.svg" />
                    <p class="formations_a">Commerce international</p>
                </div>
                <div class="formations_container_one_formation">
                    <img  src="<?php echo bloginfo('template_url'); ?>/assets/images/ComptabiliteGestion.svg" />
                    <p class="formations_a">Comptabilité Gestion</p>
                </div>
                <div class="formations_container_one_formation">
                    <img  src="<?php echo bloginfo('template_url'); ?>/assets/images/HotellerieRestauration.svg" />
                    <p class="formations_a">hôtellerie Restauration</p>
                </div>
                <div class="formations_container_one_formation">
                    <img  src="<?php echo bloginfo('template_url'); ?>/assets/images/Immobilier.svg" />
                    <p class="formations_a">Immobilier</p>
                </div>
                <div class="formations_container_one_formation">
                    <img  src="<?php echo bloginfo('template_url'); ?>/assets/images/Logistique.svg" />
                    <p class="formations_a">Logistique</p>
                </div>
                <div class="formations_container_one_formation">
                    <img  src="<?php echo bloginfo('template_url'); ?>/assets/images/Qualite.svg" />
                    <p class="formations_a">Marketing</p>
                </div>
                <div class="formations_container_one_formation">
                    <img  src="<?php echo bloginfo('template_url'); ?>/assets/images/orangeIcone.svg" />
                    <p class="formations_a">Ressources Humaines Paie</p>
                </div>
                <div class="formations_container_one_formation">
                    <img  src="<?php echo bloginfo('template_url'); ?>/assets/images/webDigital.svg" />
                    <p class="formations_a">Web Digital</p>
                </div>
                <div class="formations_container_one_formation">
                    <img  src="<?php echo bloginfo('template_url'); ?>/assets/images/webDigital.svg" />
                    <p class="formations_a">Développement Web</p>
                </div>
            </div>
        </div>
        <div class="slider">
            <div class="slider_header">
                <p class="slider_header_title" >Nos formations professionnelles les plus populaires</p>
                <div class="slider_header_arrows">
                    <img id="left_arrow" class="left_arrow" src="<?php echo bloginfo('template_url'); ?>/assets/images/right_arrow.svg" onclick="prevFormations()" alt="" srcset="">
                    <img id="right_arrow" src="<?php echo bloginfo('template_url'); ?>/assets/images/right_arrow.svg"  onclick="nextFormations()" alt="" srcset="">
                </div>
            </div>
            
            <div class="slider_cards">
                <div class="slider_card card_desktop" style="display: block !important;">
                    <div class="slider_card_header">
                        <p class="slider_card_header_title">Formation métier</p>
                    </div>
                    <div class="slider_card_body">
                        <span class="slider_card_body_span">Qualité</span>
                        <p class="slider_card_body_title">Responsable système QHSE</p>
                        <div class="slider_card_body_line">
                            <img src="<?php echo bloginfo('template_url'); ?>/assets/images/duree.svg" alt="" srcset="">
                            <p class="slider_card_body_line_p">5 à 12 mois</p>
                        </div>
                        <div class="slider_card_body_line">
                            <img src="<?php echo bloginfo('template_url'); ?>/assets/images/rncp.svg" alt="" srcset="">
                            <p class="slider_card_body_line_p">RNCP niveau 6 (bac +3/4)</p>
                        </div>
                        <div class="slider_card_body_line">
                            <img src="<?php echo bloginfo('template_url'); ?>/assets/images/location.svg" alt="" srcset="">
                            <p class="slider_card_body_line_p">En centre, à distance, en alternance</p>
                        </div>
                        <div class="slider_card_body_line">
                            <img src="<?php echo bloginfo('template_url'); ?>/assets/images/user.svg" alt="" srcset="">
                            <p class="slider_card_body_line_p">Demandeur d'emploi, salarié, étudiant</p>
                        </div>
                        <div class="slider_card_body_line">
                            <img src="<?php echo bloginfo('template_url'); ?>/assets/images/price.svg" alt="" srcset="">
                            <p class="slider_card_body_line_p">Eligible CPF</p>
                        </div>
                    </div>
                </div>

                <div class="slider_card card_desktop" style="display: block !important;">
                    <div class="slider_card_header" style="background-color: #F56B8F !important; background-image: url('/app/themes/inte/assets/images/orangeCardHeader.svg') !important;">
                        <p class="slider_card_header_title">Formation métier</p>
                    </div>
                    <div class="slider_card_body">
                        <span class="slider_card_body_span" style="background-color: #E55B2C;">Ressources Humaines Paie</span>
                        <p class="slider_card_body_title">Gestionnaire de paie</p>
                        <div class="slider_card_body_line">
                            <img src="<?php echo bloginfo('template_url'); ?>/assets/images/duree.svg" alt="" srcset="">
                            <p class="slider_card_body_line_p">5 à 12 mois</p>
                        </div>
                        <div class="slider_card_body_line">
                            <img src="<?php echo bloginfo('template_url'); ?>/assets/images/rncp.svg" alt="" srcset="">
                            <p class="slider_card_body_line_p">RNCP niveau 6 (bac +3/4)</p>
                        </div>
                        <div class="slider_card_body_line">
                            <img src="<?php echo bloginfo('template_url'); ?>/assets/images/location.svg" alt="" srcset="">
                            <p class="slider_card_body_line_p">En centre, à distance, en alternance</p>
                        </div>
                        <div class="slider_card_body_line">
                            <img src="<?php echo bloginfo('template_url'); ?>/assets/images/user.svg" alt="" srcset="">
                            <p class="slider_card_body_line_p">Demandeur d'emploi, salarié, étudiant</p>
                        </div>
                        <div class="slider_card_body_line">
                            <img src="<?php echo bloginfo('template_url'); ?>/assets/images/price.svg" alt="" srcset="">
                            <p class="slider_card_body_line_p">Eligible CPF</p>
                        </div>
                    </div>
                </div>

                <div class="slider_card card_desktop" style="display: block !important;">
                    <div class="slider_card_header" style="background-color: #3E9DD3 !important; background-image: url('/app/themes/inte/assets/images/blueCardHeader.svg') !important;">
                        <p class="slider_card_header_title">Formation métier</p>
                    </div>
                    <div class="slider_card_body">
                        <span class="slider_card_body_span" style="background-color: #3E6DB3;">Immobilier</span>
                        <p class="slider_card_body_title">Négociateur immobilier</p>
                        <div class="slider_card_body_line">
                            <img src="<?php echo bloginfo('template_url'); ?>/assets/images/duree.svg" alt="" srcset="">
                            <p class="slider_card_body_line_p">5 à 12 mois</p>
                        </div>
                        <div class="slider_card_body_line">
                            <img src="<?php echo bloginfo('template_url'); ?>/assets/images/rncp.svg" alt="" srcset="">
                            <p class="slider_card_body_line_p">RNCP niveau 6 (bac +3/4)</p>
                        </div>
                        <div class="slider_card_body_line">
                            <img src="<?php echo bloginfo('template_url'); ?>/assets/images/location.svg" alt="" srcset="">
                            <p class="slider_card_body_line_p">En centre, à distance, en alternance</p>
                        </div>
                        <div class="slider_card_body_line">
                            <img src="<?php echo bloginfo('template_url'); ?>/assets/images/user.svg" alt="" srcset="">
                            <p class="slider_card_body_line_p">Demandeur d'emploi, salarié, étudiant</p>
                        </div>
                        <div class="slider_card_body_line">
                            <img src="<?php echo bloginfo('template_url'); ?>/assets/images/price.svg" alt="" srcset="">
                            <p class="slider_card_body_line_p">Eligible CPF</p>
                        </div>
                    </div>
                </div>

                <div class="slider_card card_desktop">
                    <div class="slider_card_header" style="background-color: #3E9DD3 !important; background-image: url('/app/themes/inte/assets/images/blueCardHeader.svg') !important;">
                        <p class="slider_card_header_title">Formation métier</p>
                    </div>
                    <div class="slider_card_body">
                        <span class="slider_card_body_span" style="background-color: #3E6DB3;">Immobilier</span>
                        <p class="slider_card_body_title">Négociateur immobilier 2</p>
                        <div class="slider_card_body_line">
                            <img src="<?php echo bloginfo('template_url'); ?>/assets/images/duree.svg" alt="" srcset="">
                            <p class="slider_card_body_line_p">5 à 12 mois</p>
                        </div>
                        <div class="slider_card_body_line">
                            <img src="<?php echo bloginfo('template_url'); ?>/assets/images/rncp.svg" alt="" srcset="">
                            <p class="slider_card_body_line_p">RNCP niveau 6 (bac +3/4)</p>
                        </div>
                        <div class="slider_card_body_line">
                            <img src="<?php echo bloginfo('template_url'); ?>/assets/images/location.svg" alt="" srcset="">
                            <p class="slider_card_body_line_p">En centre, à distance, en alternance</p>
                        </div>
                        <div class="slider_card_body_line">
                            <img src="<?php echo bloginfo('template_url'); ?>/assets/images/user.svg" alt="" srcset="">
                            <p class="slider_card_body_line_p">Demandeur d'emploi, salarié, étudiant</p>
                        </div>
                        <div class="slider_card_body_line">
                            <img src="<?php echo bloginfo('template_url'); ?>/assets/images/price.svg" alt="" srcset="">
                            <p class="slider_card_body_line_p">Eligible CPF</p>
                        </div>
                    </div>
                </div>

                <div class="slider_card card_desktop">
                    <div class="slider_card_header">
                        <p class="slider_card_header_title">Formation métier</p>
                    </div>
                    <div class="slider_card_body">
                        <span class="slider_card_body_span">Qualité</span>
                        <p class="slider_card_body_title">Responsable système QHSE 2</p>
                        <div class="slider_card_body_line">
                            <img src="<?php echo bloginfo('template_url'); ?>/assets/images/duree.svg" alt="" srcset="">
                            <p class="slider_card_body_line_p">5 à 12 mois</p>
                        </div>
                        <div class="slider_card_body_line">
                            <img src="<?php echo bloginfo('template_url'); ?>/assets/images/rncp.svg" alt="" srcset="">
                            <p class="slider_card_body_line_p">RNCP niveau 6 (bac +3/4)</p>
                        </div>
                        <div class="slider_card_body_line">
                            <img src="<?php echo bloginfo('template_url'); ?>/assets/images/location.svg" alt="" srcset="">
                            <p class="slider_card_body_line_p">En centre, à distance, en alternance</p>
                        </div>
                        <div class="slider_card_body_line">
                            <img src="<?php echo bloginfo('template_url'); ?>/assets/images/user.svg" alt="" srcset="">
                            <p class="slider_card_body_line_p">Demandeur d'emploi, salarié, étudiant</p>
                        </div>
                        <div class="slider_card_body_line">
                            <img src="<?php echo bloginfo('template_url'); ?>/assets/images/price.svg" alt="" srcset="">
                            <p class="slider_card_body_line_p">Eligible CPF</p>
                        </div>
                    </div>
                </div>

                <div class="slider_card card_desktop">
                    <div class="slider_card_header">
                        <p class="slider_card_header_title">Formation métier</p>
                    </div>
                    <div class="slider_card_body">
                        <span class="slider_card_body_span">Qualité</span>
                        <p class="slider_card_body_title">Responsable système QHSE 2</p>
                        <div class="slider_card_body_line">
                            <img src="<?php echo bloginfo('template_url'); ?>/assets/images/duree.svg" alt="" srcset="">
                            <p class="slider_card_body_line_p">5 à 12 mois</p>
                        </div>
                        <div class="slider_card_body_line">
                            <img src="<?php echo bloginfo('template_url'); ?>/assets/images/rncp.svg" alt="" srcset="">
                            <p class="slider_card_body_line_p">RNCP niveau 6 (bac +3/4)</p>
                        </div>
                        <div class="slider_card_body_line">
                            <img src="<?php echo bloginfo('template_url'); ?>/assets/images/location.svg" alt="" srcset="">
                            <p class="slider_card_body_line_p">En centre, à distance, en alternance</p>
                        </div>
                        <div class="slider_card_body_line">
                            <img src="<?php echo bloginfo('template_url'); ?>/assets/images/user.svg" alt="" srcset="">
                            <p class="slider_card_body_line_p">Demandeur d'emploi, salarié, étudiant</p>
                        </div>
                        <div class="slider_card_body_line">
                            <img src="<?php echo bloginfo('template_url'); ?>/assets/images/price.svg" alt="" srcset="">
                            <p class="slider_card_body_line_p">Eligible CPF</p>
                        </div>
                    </div>
                </div>
            </div> 

            <div class="slider_card fade slider_card_mobile" style="display: block;">
                <div class="slider_card_header">
                    <p class="slider_card_header_title">Formation métier</p>
                </div>
                <div class="slider_card_body">
                    <span class="slider_card_body_span">Qualité</span>
                    <p class="slider_card_body_title">Responsable système QHSE</p>
                    <div class="slider_card_body_line">
                        <img src="<?php echo bloginfo('template_url'); ?>/assets/images/duree.svg" alt="" srcset="">
                        <p class="slider_card_body_line_p">5 à 12 mois</p>
                    </div>
                    <div class="slider_card_body_line">
                        <img src="<?php echo bloginfo('template_url'); ?>/assets/images/rncp.svg" alt="" srcset="">
                        <p class="slider_card_body_line_p">RNCP niveau 6 (bac +3/4)</p>
                    </div>
                    <div class="slider_card_body_line">
                        <img src="<?php echo bloginfo('template_url'); ?>/assets/images/location.svg" alt="" srcset="">
                        <p class="slider_card_body_line_p">En centre, à distance, en alternance</p>
                    </div>
                    <div class="slider_card_body_line">
                        <img src="<?php echo bloginfo('template_url'); ?>/assets/images/user.svg" alt="" srcset="">
                        <p class="slider_card_body_line_p">Demandeur d'emploi, salarié, étudiant</p>
                    </div>
                    <div class="slider_card_body_line">
                        <img src="<?php echo bloginfo('template_url'); ?>/assets/images/price.svg" alt="" srcset="">
                        <p class="slider_card_body_line_p">Eligible CPF</p>
                    </div>
                </div>
            </div>

            <div class="slider_card fade slider_card_mobile">
                <div class="slider_card_header" style="background-color: #F56B8F !important; background-image: url('/app/themes/inte/assets/images/orangeCardHeader.svg') !important;">
                    <p class="slider_card_header_title">Formation métier</p>
                </div>
                <div class="slider_card_body">
                    <span class="slider_card_body_span" style="background-color: #E55B2C;">Ressources Humaines Paie</span>
                    <p class="slider_card_body_title">Gestionnaire de paie</p>
                    <div class="slider_card_body_line">
                        <img src="<?php echo bloginfo('template_url'); ?>/assets/images/duree.svg" alt="" srcset="">
                        <p class="slider_card_body_line_p">5 à 12 mois</p>
                    </div>
                    <div class="slider_card_body_line">
                        <img src="<?php echo bloginfo('template_url'); ?>/assets/images/rncp.svg" alt="" srcset="">
                        <p class="slider_card_body_line_p">RNCP niveau 6 (bac +3/4)</p>
                    </div>
                    <div class="slider_card_body_line">
                        <img src="<?php echo bloginfo('template_url'); ?>/assets/images/location.svg" alt="" srcset="">
                        <p class="slider_card_body_line_p">En centre, à distance, en alternance</p>
                    </div>
                    <div class="slider_card_body_line">
                        <img src="<?php echo bloginfo('template_url'); ?>/assets/images/user.svg" alt="" srcset="">
                        <p class="slider_card_body_line_p">Demandeur d'emploi, salarié, étudiant</p>
                    </div>
                    <div class="slider_card_body_line">
                        <img src="<?php echo bloginfo('template_url'); ?>/assets/images/price.svg" alt="" srcset="">
                        <p class="slider_card_body_line_p">Eligible CPF</p>
                    </div>
                </div>
            </div>

            <div class="slider_card fade slider_card_mobile">
                <div class="slider_card_header" style="background-color: #3E9DD3 !important; background-image: url('/app/themes/inte/assets/images/blueCardHeader.svg') !important;">
                    <p class="slider_card_header_title">Formation métier</p>
                </div>
                <div class="slider_card_body">
                    <span class="slider_card_body_span" style="background-color: #3E6DB3;">Immobilier</span>
                    <p class="slider_card_body_title">Négociateur immobilier</p>
                    <div class="slider_card_body_line">
                        <img src="<?php echo bloginfo('template_url'); ?>/assets/images/duree.svg" alt="" srcset="">
                        <p class="slider_card_body_line_p">5 à 12 mois</p>
                    </div>
                    <div class="slider_card_body_line">
                        <img src="<?php echo bloginfo('template_url'); ?>/assets/images/rncp.svg" alt="" srcset="">
                        <p class="slider_card_body_line_p">RNCP niveau 6 (bac +3/4)</p>
                    </div>
                    <div class="slider_card_body_line">
                        <img src="<?php echo bloginfo('template_url'); ?>/assets/images/location.svg" alt="" srcset="">
                        <p class="slider_card_body_line_p">En centre, à distance, en alternance</p>
                    </div>
                    <div class="slider_card_body_line">
                        <img src="<?php echo bloginfo('template_url'); ?>/assets/images/user.svg" alt="" srcset="">
                        <p class="slider_card_body_line_p">Demandeur d'emploi, salarié, étudiant</p>
                    </div>
                    <div class="slider_card_body_line">
                        <img src="<?php echo bloginfo('template_url'); ?>/assets/images/price.svg" alt="" srcset="">
                        <p class="slider_card_body_line_p">Eligible CPF</p>
                    </div>
                </div>
            </div>

            <div class="slider_card fade slider_card_mobile">
                <div class="slider_card_header">
                    <p class="slider_card_header_title">Formation métier</p>
                </div>
                <div class="slider_card_body">
                    <span class="slider_card_body_span">Qualité</span>
                    <p class="slider_card_body_title">Responsable système QHSE</p>
                    <div class="slider_card_body_line">
                        <img src="<?php echo bloginfo('template_url'); ?>/assets/images/duree.svg" alt="" srcset="">
                        <p class="slider_card_body_line_p">5 à 12 mois</p>
                    </div>
                    <div class="slider_card_body_line">
                        <img src="<?php echo bloginfo('template_url'); ?>/assets/images/rncp.svg" alt="" srcset="">
                        <p class="slider_card_body_line_p">RNCP niveau 6 (bac +3/4)</p>
                    </div>
                    <div class="slider_card_body_line">
                        <img src="<?php echo bloginfo('template_url'); ?>/assets/images/location.svg" alt="" srcset="">
                        <p class="slider_card_body_line_p">En centre, à distance, en alternance</p>
                    </div>
                    <div class="slider_card_body_line">
                        <img src="<?php echo bloginfo('template_url'); ?>/assets/images/user.svg" alt="" srcset="">
                        <p class="slider_card_body_line_p">Demandeur d'emploi, salarié, étudiant</p>
                    </div>
                    <div class="slider_card_body_line">
                        <img src="<?php echo bloginfo('template_url'); ?>/assets/images/price.svg" alt="" srcset="">
                        <p class="slider_card_body_line_p">Eligible CPF</p>
                    </div>
                </div>
            </div>

            <div class="slider_card fade slider_card_mobile">
                <div class="slider_card_header" style="background-color: #F56B8F !important; background-image: url('/app/themes/inte/assets/images/orangeCardHeader.svg') !important;">
                    <p class="slider_card_header_title">Formation métier</p>
                </div>
                <div class="slider_card_body">
                    <span class="slider_card_body_span" style="background-color: #E55B2C;">Ressources Humaines Paie</span>
                    <p class="slider_card_body_title">Gestionnaire de paie</p>
                    <div class="slider_card_body_line">
                        <img src="<?php echo bloginfo('template_url'); ?>/assets/images/duree.svg" alt="" srcset="">
                        <p class="slider_card_body_line_p">5 à 12 mois</p>
                    </div>
                    <div class="slider_card_body_line">
                        <img src="<?php echo bloginfo('template_url'); ?>/assets/images/rncp.svg" alt="" srcset="">
                        <p class="slider_card_body_line_p">RNCP niveau 6 (bac +3/4)</p>
                    </div>
                    <div class="slider_card_body_line">
                        <img src="<?php echo bloginfo('template_url'); ?>/assets/images/location.svg" alt="" srcset="">
                        <p class="slider_card_body_line_p">En centre, à distance, en alternance</p>
                    </div>
                    <div class="slider_card_body_line">
                        <img src="<?php echo bloginfo('template_url'); ?>/assets/images/user.svg" alt="" srcset="">
                        <p class="slider_card_body_line_p">Demandeur d'emploi, salarié, étudiant</p>
                    </div>
                    <div class="slider_card_body_line">
                        <img src="<?php echo bloginfo('template_url'); ?>/assets/images/price.svg" alt="" srcset="">
                        <p class="slider_card_body_line_p">Eligible CPF</p>
                    </div>
                </div>
            </div>

            <div class="slider_card fade slider_card_mobile">
                <div class="slider_card_header" style="background-color: #3E9DD3 !important; background-image: url('/app/themes/inte/assets/images/blueCardHeader.svg') !important;">
                    <p class="slider_card_header_title">Formation métier</p>
                </div>
                <div class="slider_card_body">
                    <span class="slider_card_body_span" style="background-color: #3E6DB3;">Immobilier</span>
                    <p class="slider_card_body_title">Négociateur immobilier</p>
                    <div class="slider_card_body_line">
                        <img src="<?php echo bloginfo('template_url'); ?>/assets/images/duree.svg" alt="" srcset="">
                        <p class="slider_card_body_line_p">5 à 12 mois</p>
                    </div>
                    <div class="slider_card_body_line">
                        <img src="<?php echo bloginfo('template_url'); ?>/assets/images/rncp.svg" alt="" srcset="">
                        <p class="slider_card_body_line_p">RNCP niveau 6 (bac +3/4)</p>
                    </div>
                    <div class="slider_card_body_line">
                        <img src="<?php echo bloginfo('template_url'); ?>/assets/images/location.svg" alt="" srcset="">
                        <p class="slider_card_body_line_p">En centre, à distance, en alternance</p>
                    </div>
                    <div class="slider_card_body_line">
                        <img src="<?php echo bloginfo('template_url'); ?>/assets/images/user.svg" alt="" srcset="">
                        <p class="slider_card_body_line_p">Demandeur d'emploi, salarié, étudiant</p>
                    </div>
                    <div class="slider_card_body_line">
                        <img src="<?php echo bloginfo('template_url'); ?>/assets/images/price.svg" alt="" srcset="">
                        <p class="slider_card_body_line_p">Eligible CPF</p>
                    </div>
                </div>
            </div>

            <div class="slider_dots">
                <span class="slider_dot active" onclick="currentSlide(1)"></span>
                <span class="slider_dot" onclick="currentSlide(2)"></span>
                <span class="slider_dot" onclick="currentSlide(3)"></span>
                <span class="slider_dot" onclick="currentSlide(4)"></span>
                <span class="slider_dot" onclick="currentSlide(5)"></span>
                <span class="slider_dot" onclick="currentSlide(6)"></span>
            </div>
            <div class="slider_footer">
                <button class="slider_button" >Parcourir toutes nos formations</button>
            </div>
        </div>
        <?php wp_footer(); ?>
    </body>
</html>
