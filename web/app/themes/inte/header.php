<!doctype html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo('charset'); ?>">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php wp_head(); ?>
    </head>
    <body <?php body_class(); ?>>

    <?php wp_body_open(); ?>

    <header class="ifocop">
        <nav class="ifocop_header">
            <div class="ifocop_logo">
                <!-- Top Navigation Menu -->
                <div class="ifocop_nav">
                    <a id="popup_menu" href="javascript:void(0);" class="icon" onclick="show_hide_pop_menu()">
                        <img src="<?php echo bloginfo('template_url'); ?>/assets/images/menu.svg" /> 
                    </a>
                </div>
                <a href="#">
                    <img src="<?php echo bloginfo('template_url'); ?>/assets/images/Logo.svg" />  
                </a>
            </div>
            <div>
                <input class="ifocop_search_input_horizontal" type="search" id="mySearch" placeholder="Rechercher" title="Type in a category">
            </div>
            <div class="ifocop_header_right_container">
                <button class="ifocop_button_header">
                    Espace apprenant
                </button>
                <p class="ifocop_tel">
                    01 56 34 69 69
                </p>
                <a class="ifocop_search_button" href="javascript:void(0);"  onclick="show_hide_search_header()">
                    <img src="<?php echo bloginfo('template_url'); ?>/assets/images/search.svg" /> 
                </a>

                <div class="ifocop_tel_container">
                    <p class="ifocop_p1" >Un projet de formation ?</p>
                    <p class="ifocop_p2" >01 56 34 69 69</p>
                </div>
            </div>
        </nav>
        <div class="ifocop_menu_desktop">
            <a href="#" class="ifocop_first_item_desktop">Vous êtes</a>
            <a href="#" class="ifocop_item_desktop">Domaines de formation</a>
            <a href="#" class="ifocop_item_desktop">Nos formules</a>
            <a href="#" class="ifocop_item_desktop">Financement</a>
            <a href="#" class="ifocop_item_desktop">Nos centres</a>
            <a href="#" class="ifocop_item_desktop">ifocop</a>
            <a href="#" class="ifocop_item_desktop">Blog</a>
        </div>
        <div id="myLinks" class="ifocop_pop_menu_link">
            <div class="ifocop_side_logo" >
                <a class="ifocop_plus" >
                    <img src="<?php echo bloginfo('template_url'); ?>/assets/images/greenLogo.svg" />
                </a>
                <a>
                    <img class="ifocop_exit" src="<?php echo bloginfo('template_url'); ?>/assets/images/exit.svg"  onclick="show_hide_pop_menu()"/>
                </a>
            </div>
            <br/>
            <div class="ifocop_plus">
                <a class="ifocop_a" href="#">Vous êtes</a>
                <img  src="<?php echo bloginfo('template_url'); ?>/assets/images/plus.svg" />
            </div> 
            <hr/>
            <div class="ifocop_plus" href="javascript:void(0);" onclick="show_hide_sub_menu()">
                <a class="ifocop_a" >Domaines de formation</a>
                <img id="plus_icon" src="<?php echo bloginfo('template_url'); ?>/assets/images/plus.svg" />
            </div>
            <hr/>
            <div id="links_for_domaines" class="ifocop_sub_menu_link">
                <div class="ifocop_sub_item">
                    <img  src="<?php echo bloginfo('template_url'); ?>/assets/images/Achats.svg" />
                    <a class="ifocop_a" href="#">Achats</a>
                </div>
                <div class="ifocop_sub_item">
                    <img  src="<?php echo bloginfo('template_url'); ?>/assets/images/Assistanat_Secretariat.svg" />
                    <a class="ifocop_a" href="#">Assistanat Secretariat</a>
                </div>
                <div class="ifocop_sub_item">
                    <img  src="<?php echo bloginfo('template_url'); ?>/assets/images/Commerce.svg" />
                    <a class="ifocop_a" href="#">Commerce</a>
                </div>
                <div class="ifocop_sub_item">
                    <img  src="<?php echo bloginfo('template_url'); ?>/assets/images/Commerce.svg" />
                    <a class="ifocop_a" href="#">Commerce International</a>
                </div>
                <div class="ifocop_sub_item">
                    <img  src="<?php echo bloginfo('template_url'); ?>/assets/images/ComptabiliteGestion.svg" />
                    <a class="ifocop_a" href="#">Comptabilité Gestion</a>
                </div>
                <div class="ifocop_sub_item">
                    <img  src="<?php echo bloginfo('template_url'); ?>/assets/images/HotellerieRestauration.svg" />
                    <a class="ifocop_a" href="#">hôtellerie Restauration</a>
                </div>
                <div class="ifocop_sub_item">
                    <img  src="<?php echo bloginfo('template_url'); ?>/assets/images/Immobilier.svg" />
                    <a class="ifocop_a" href="#">Immobilier</a>
                </div>
                <div class="ifocop_sub_item">
                    <img  src="<?php echo bloginfo('template_url'); ?>/assets/images/Logistique.svg" />
                    <a class="ifocop_a" href="#">Logistique</a>
                </div>
                <div class="ifocop_sub_item">
                    <img  src="<?php echo bloginfo('template_url'); ?>/assets/images/Qualite.svg" />
                    <a class="ifocop_a" href="#">Marketing</a>
                </div>
                <div class="ifocop_sub_item">
                    <img  src="<?php echo bloginfo('template_url'); ?>/assets/images/orangeIcone.svg" />
                    <a class="ifocop_a" href="#">Ressources Humaines Paie</a>             
                </div>
            </div>
            <div class="ifocop_plus">
                <a class="ifocop_a" href="#">Nos formules</a>
                <img  src="<?php echo bloginfo('template_url'); ?>/assets/images/plus.svg" />
            </div>
            <hr/>
            <div class="ifocop_plus">
                <a class="ifocop_a" href="#">Financement</a>
                <img  src="<?php echo bloginfo('template_url'); ?>/assets/images/plus.svg" />
            </div>
            <hr/>
            <div class="ifocop_plus">    
                <a class="ifocop_a" href="#">Nos centres</a>
                <img  src="<?php echo bloginfo('template_url'); ?>/assets/images/plus.svg" />
            </div>
            <hr/>
            <div class="ifocop_plus">
                <a class="ifocop_a" href="#">ifocop</a>
                <img  src="<?php echo bloginfo('template_url'); ?>/assets/images/plus.svg" />
            </div>
            <hr/>
            <div class="ifocop_plus">
                <a class="ifocop_a" href="#">Blog</a>
                <img  src="<?php echo bloginfo('template_url'); ?>/assets/images/plus.svg" />
            </div>
            <div style="display: grid; align-items: center; margin: 0 25%">
                <button class="ifocop_button_nav">Espace apprenant</button>
            </div>
           
        </div>
    </header>
    <div id="search_header"  class="ifocop_search">
        <input class="ifocop_search_input" type="search" id="mySearch" placeholder="Rechercher" title="Type in a category">
    </div>
   