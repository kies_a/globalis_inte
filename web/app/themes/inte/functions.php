<?php
function assetURL($file, $versioning = false)
{
    $path = get_template_directory_uri() . '/dist/' . $file;

    if ($versioning) {
        $version = getAssetsVersion();

        if (false != $version && !empty($version)) {
            $path = str_replace(['.css', '.js'], ['-' . $version . '.css', '-' . $version . '.js'], $path);
        }
    }

    return $path;
}

function getAssetsVersion()
{
    static $version;

    if (!isset($version)) {
        $version_path = get_template_directory() . '/dist/version';
        if (!file_exists($version_path)) {
            $version = false;
        } else {
            $version = intval(file_get_contents($version_path));
        }
    }

    return $version;
}

// Ajouter la prise en charge des images mises en avant
add_theme_support('post-thumbnails');

add_theme_support('title-tag');

add_action('wp_enqueue_scripts', function() {
    wp_enqueue_style('inte/main', assetURL('styles/main.css', ASSETS_VERSIONING_STYLES));
}, 100);

add_action('wp_enqueue_scripts', function() {
    wp_enqueue_script('inte/main', assetURL('scripts/main.js', ASSETS_VERSIONING_STYLES));
}, 100);